const flashData = $('.flash-data').data('flashdata');

if (flashData) {
    Swal.fire({
        title: 'Data Task',
        text: 'Succesfully ' + flashData,
        type: 'success',
        showCancelButton: false,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#"
    })
}