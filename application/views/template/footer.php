<footer class="footer">
    Copyright &copy; <?= date('Y'); ?> <span class="d-none d-sm-inline-block">- Crafted with <i class="mdi mdi-heart text-danger"></i> by Agus Dwi Ismawan</span>.
</footer>
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
</div><!-- END wrapper -->
<!-- jQuery  -->
<script src="<?= base_url('assets/'); ?>js/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/metisMenu.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/jquery.slimscroll.js"></script>
<script src="<?= base_url('assets/'); ?>js/waves.min.js"></script>
<!-- peity JS -->
<script src="<?= base_url('assets/'); ?>pages/dashboard.js"></script>
<!-- App js -->
<script src="<?= base_url('assets/'); ?>js/app.js"></script>
<!-- Max Length -->
<script src="<?= base_url('assets/'); ?>plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="<?= base_url('assets/'); ?>pages/form-advanced.js"></script>
<!-- Sweet-Alert  -->
<script src="<?= base_url('assets/'); ?>plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/myscript.js"></script>

<!-- Form Xeditable -->
<script src="<?= base_url('assets/'); ?>plugins/x-editable/js/bootstrap-editable.min.js"></script>
<script src="<?= base_url('assets/'); ?>pages/xeditable.js"></script>

</body>
<!-- Mirrored from themesbrand.com/veltrix/layouts/vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Jul 2019 04:37:41 GMT -->

</html>