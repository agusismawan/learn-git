<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Main</li>
                <li><a href="<?= base_url('welcome'); ?>" class="waves-effect"><i class="ti-home"></i> <span>Dashboard</span></a></li>
                <li><a href="<?= base_url('todo/todoapp') ?>" class="waves-effect"><i class="ti-view-list-alt"></i> <span>Todo</span></a></li>
                <!-- <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-view-list-alt"></i><span> Todo <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                    <ul class="submenu">
                        <li><a href="email-inbox.html">Task List</a></li>
                        <li><a href="email-read.html">Task Done</a></li>
                        <li><a href="email-compose.html">Task Archive</a></li>
                    </ul>
                </li> -->
            </ul>
        </div><!-- Sidebar -->
        <div class="clearfix"></div>
    </div><!-- Sidebar -left -->
</div><!-- Left Sidebar End -->