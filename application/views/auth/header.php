<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from themesbrand.com/veltrix/layouts/vertical/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Jul 2019 04:38:10 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title><?= $title; ?></title>
    <meta content="Admin Dashboard" name="description">
    <meta content="Themesbrand" name="author">
    <link rel="shortcut icon" href="<?= base_url('assets/'); ?>images/favicon.ico">

    <link href="<?= base_url('assets/'); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/'); ?>css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/'); ?>css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/'); ?>css/style.css" rel="stylesheet" type="text/css">

</head>

<body>