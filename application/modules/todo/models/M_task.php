<?php

class M_task extends CI_Model
{
    public function addTask()
    {
        $data = [
            'task_name' => $this->input->post('task_name'),
            'task_description' => $this->input->post('description'),
            'task_duedate' => $this->input->post('due_date'),
            'task_deleted' => 0,
            'task_date_added' => date('Y-m-d H:i:s')
        ];

        $this->db->insert('t_task', $data);
    }

    public function getAllTask()
    {
        $this->db->order_by('task_date_added', 'DESC');
        $this->db->where('task_deleted', 0);
        return $this->db->get('t_task');
    }

    public function editTask()
    {
        $data = [
            'task_name' => $this->input->post('task_name'),
            'task_description' => $this->input->post('description'),
            'task_duedate' => $this->input->post('due_date'),
            'task_deleted' => 0,
        ];

        $this->db->where('task_id', $this->input->post('id'));
        $this->db->update('t_task', $data);
    }
}
