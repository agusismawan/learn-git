<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <h4 class="page-title">Todo Apps</h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Welcome to Todo Apps</li>
                        </ol>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div><!-- end row -->

            <div class="row">
                <div class="col-xl-8">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title mb-4">Task List</h4>
                            <?php if (validation_errors()) : ?>
                                <div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button> <?= validation_errors(); ?>
                                </div>
                            <?php endif; ?>
                            <div class="flash-data" data-flashdata="<?= $this->session->flashdata('message'); ?>"></div>
                            <?php if ($this->session->flashdata('message')) {
                                $this->session->flashdata('message');
                            } ?>
                            <button type="button" class="mb-2 btn btn-primary btn-sm waves-effect waves-light" data-toggle="modal" data-target="#myModal">Standard Modal</button>

                            <div id="accordion">
                                <?php foreach ($task as $t) : ?>
                                    <div class="card mb-1">
                                        <div class="card-header p-3" id="heading<?= $t['task_id'] ?>">
                                            <h6 class="m-0 font-14">
                                                <a href="#collapse<?= $t['task_id'] ?>" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="collapse<?= $t['task_id'] ?>"><?= $t['task_name']; ?></a>
                                                <a href="" class="tsk float-right"><i class="ti-close"></i></a>
                                                <a href="" class="tsk float-right" data-toggle="modal" data-target="#mdlEdit<?= $t['task_id']; ?>"><i class="ti-pencil"></i></a>
                                            </h6>
                                        </div>
                                        <div id="collapse<?= $t['task_id'] ?>" class="collapse" aria-labelledby="heading<?= $t['task_id'] ?>" data-parent="#accordion">
                                            <div class="card-body">
                                                <i class="ti-write">&nbsp;&nbsp;</i><?= $t['task_description']; ?>
                                                <br>
                                                <i class="ti-calendar">&nbsp;&nbsp;</i><?= $t['task_duedate']; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!-- end row -->

            <div class="row">
                <div class="col-xl-8">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title mb-4">Task Progress</h4>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">(#)</th>
                                            <th scope="col">Action</th>
                                            <th scope="col">Task</th>
                                            <th scope="col">Description</th>
                                            <th scope="col" colspan="2">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($task as $tb) : ?>
                                            <tr>
                                                <th scope="row"><?= $i++; ?></th>
                                                <td>
                                                    <div><a href="#" class="btn btn-primary btn-sm">Edit</a></div>
                                                </td>
                                                <td><?= $tb['task_name']; ?></td>
                                                <td><?= $tb['task_description']; ?></td>
                                                <td>
                                                    <span class="badge badge-success"><?= $tb['task_status']; ?></span>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->
</div>

<!-- Modal Tambah -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Add New Task</h5><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="<?= base_url('todo/todoapp/index'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="task_name" class="col-sm-3 col-form-label">Task</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" id="task_name" name="task_name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-9">
                            <textarea id="textarea" name="description" class="form-control" maxlength="225" rows="3" placeholder="This textarea has a limit of 225 chars."></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="due_date" class="col-sm-3 col-form-label">Due Date</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="datetime-local" id="due_date" name="due_date">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal Ubah -->
<?php foreach ($task as $key) : ?>
    <div id="mdlEdit<?= $key['task_id']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mdlEditLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="mdlEditLabel">Edit Task</h5><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form action="<?= base_url('todo/todoapp/edit'); ?>" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="id" value="<?= $key['task_id']; ?>">
                        <div class="form-group row">
                            <label for="task_name" class="col-sm-3 col-form-label">Task</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" id="task_name" name="task_name" value="<?= $key['task_name']; ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-3 col-form-label">Description</label>
                            <div class="col-sm-9">
                                <textarea id="textarea" name="description" class="form-control" maxlength="225" rows="3" placeholder="This textarea has a limit of 225 chars." required><?= $key['task_description']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="due_date" class="col-sm-3 col-form-label">Due Date</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="datetime-local" id="due_date" name="due_date" value="<?= $key['task_duedate']; ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>