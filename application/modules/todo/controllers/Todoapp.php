<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Todoapp extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_task');
    }

    public function index()
    {
        $data['title'] = 'Todo Apps';
        $data['user'] = $this->db->get_where('t_user', ['user_username' => $this->session->userdata('username')])->row_array();
        $data['task'] = $this->M_task->getAllTask()->result_array();

        $this->form_validation->set_rules('task_name', 'Task Name', 'required|trim');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('due_date', 'Due Date', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('../template/header', $data);
            $this->load->view('../template/topbar', $data);
            $this->load->view('../template/sidebar', $data);
            $this->load->view('v_todo', $data);
            $this->load->view('../template/footer');
        } else {
            $this->M_task->addTask();
            $this->session->set_flashdata('message', 'Added');
            redirect('todo/todoapp');
        }
    }

    public function edit()
    {
        $this->M_task->editTask();
        $this->session->set_flashdata('message', 'Changed');
        redirect('todo/todoapp');
    }
}
