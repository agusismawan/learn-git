<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <h4 class="page-title">Dashboard</h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Welcome to Dashboard</li>
                        </ol>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div><!-- end row -->
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card mini-stat bg-primary text-white">
                        <div class="card-body">
                            <div class="mb-4">
                                <div class="float-left mini-stat-img mr-4">
                                    <img src="assets/images/services-icon/01.png" alt="">
                                </div>
                                <h5 class="font-16 text-uppercase mt-0 text-white-50">Task List</h5>
                                <h4 class="font-500">1,685</h4>
                            </div>
                            <div class="pt-2">
                                <div class="float-right">
                                    <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                </div>
                                <p class="text-white-50 mb-0">See detail</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end row -->
            <!-- <div class="row">
               <div class="col-xl-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title mb-4">Activity</h4>
                            <ol class="activity-feed">
                                <li class="feed-item">
                                    <div class="feed-item-list">
                                        <span class="date">Jan 22</span> 
                                        <span class="activity-text">Responded to need “Volunteer Activities”</span>
                                    </div>
                                </li>
                                <li class="feed-item">
                                    <div class="feed-item-list">
                                        <span class="date">Jan 20</span> 
                                        <span class="activity-text">At vero eos et accusamus et iusto odio dignissimos ducimus qui deleniti atque...<a href="#" class="text-success">Read more</a></span>
                                    </div>
                                </li>
                                <li class="feed-item">
                                    <div class="feed-item-list">
                                        <span class="date">Jan 19</span> 
                                        <span class="activity-text">Joined the group “Boardsmanship Forum”</span>
                                    </div>
                                </li>
                                <li class="feed-item">
                                    <div class="feed-item-list">
                                        <span class="date">Jan 17</span> 
                                        <span class="activity-text">Responded to need “In-Kind Opportunity”</span>
                                    </div>
                                </li>
                                <li class="feed-item">
                                    <div class="feed-item-list">
                                        <span class="date">Jan 16</span> 
                                        <span class="activity-text">Sed ut perspiciatis unde omnis iste natus error sit rem.</span>
                                    </div>
                                </li>
                            </ol>
                                <div class="text-center">
                                    <a href="#" class="btn btn-primary">Load More</a>
                                </div>
                        </div>
                    </div>
                </div>
        </div>end row -->

        </div><!-- container-fluid -->
    </div><!-- content -->