<div class="home-btn d-none d-sm-block"><a href="index-2.html" class="text-dark"><i class="fas fa-home h2"></i></a>
    </div>
    <div class="wrapper-page">
        <div class="card overflow-hidden account-card mx-3">
            <div class="bg-primary p-4 text-white text-center position-relative">
                <h4 class="font-20 m-b-5">Free Register</h4>
                <p class="text-white-50 mb-4">Get your free account now.</p><a href="index-2.html"
                    class="logo logo-admin"><img src="<?= base_url('assets/'); ?>images/logo-sm.png" height="24" alt="logo"></a>
            </div>
            <div class="account-card-content">
                <form class="form-horizontal m-t-30" method="post" action="<?= base_url('auth/registration'); ?>">
                    <div class="form-group">
                        <label for="useremail">Email</label> 
                        <input type="email" class="form-control" id="useremail" placeholder="Enter email" name="useremail" value="<?= set_value('useremail'); ?>">
                        <?= form_error('useremail', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label> 
                        <input type="text" class="form-control" id="username" placeholder="Enter username" name="username" value="<?= set_value('username'); ?>">
                        <?= form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="fullname">Full Name</label> 
                        <input type="text" class="form-control" id="fullname" placeholder="Enter fullname" name="fullname">
                        <?= form_error('fullname', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="userpassword1">Password</label> 
                        <input type="password" class="form-control" id="userpassword1" placeholder="Enter password" name="userpassword1">
                        <?= form_error('userpassword1', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="userpassword2">Confirm Password</label> 
                        <input type="password" class="form-control" id="userpassword2" placeholder="Retype password" name="userpassword2">
                        <?= form_error('userpassword2', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group row m-t-20">
                        <div class="col-12 text-right">
                            <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Register</button>
                        </div>
                    </div>
                    <div class="form-group m-t-10 mb-0 row">
                        <div class="col-12 m-t-20">
                            <p class="mb-0">By registering you agree to the  <a href="#" class="text-primary">Terms of Use</a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="m-t-40 text-center">
            <p>Already have an account ? <a href="<?= base_url('auth'); ?>" class="font-500 text-primary">Login</a></p>
            <p>Copyright &copy; <?= date('Y'); ?>. Crafted with <i class="mdi mdi-heart text-danger"></i> by Agus Dwi Ismawan</p>
        </div>
    </div><!-- end wrapper-page -->