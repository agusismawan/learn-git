<?php

class M_auth extends CI_Model
{
    public function addNewUser()
    {
        $data = [
            'user_email' => htmlspecialchars($this->input->post('useremail', true)),
            'user_username' => htmlspecialchars($this->input->post('username', true)),
            'user_fullname' => htmlspecialchars($this->input->post('fullname', true)),
            'user_password' => password_hash($this->input->post('userpassword1'), PASSWORD_DEFAULT),
            'user_image' => 'default.jpg',
            'user_isactive' => 1,
            'user_date_created' => date('Y-m-d H:i:s')
        ];

        $this->db->insert('t_user', $data);
    }
}
