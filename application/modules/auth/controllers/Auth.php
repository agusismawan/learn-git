<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_auth');
    }

    public function index()
    {
        if ($this->session->userdata('username')) {
            redirect('welcome');
        }

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('userpassword', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Login Page';
            $this->load->view('../auth/header', $data);
            $this->load->view('v_login');
            $this->load->view('../auth/footer');
        } else {
            // validasi sukses
            $this->_login();
        }
    }

    private function _login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('userpassword');
        $user = $this->db->get_where('t_user', ['user_username' => $username])->row_array();

        // jika usernya ada
        if ($user) {
            // jika usernya aktif
            if ($user['user_isactive'] == 1) {
                // cek password
                if (password_verify($password, $user['user_password'])) {
                    $data = [
                        'username' => $user['user_username'],
                    ];
                    $this->session->set_userdata($data);
                    $this->session->set_flashdata('message', 'Welcome Back!');
                    redirect('welcome');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button> <strong>Wrong</strong> Password!
                            </div>');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button> <strong>User</strong> is not active!
                            </div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button> <strong>Username</strong> is not registered!
                            </div>');
            redirect('auth');
        }
    }

    public function registration()
    {
        $this->form_validation->set_rules('useremail', 'Email', 'required|trim|valid_email|is_unique[t_user.user_email]', [
            'is_unique' => 'This email has already registered!'
        ]);
        $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[t_user.user_username]', [
            'is_unique' => 'This username already registered!'
        ]);
        $this->form_validation->set_rules('fullname', 'Full name', 'required|trim');
        $this->form_validation->set_rules('userpassword1', 'Password', 'required|trim|min_length[6]|matches[userpassword2]', [
            'matches' => 'Password dont match!',
            'min_length' => 'Password too short!'
        ]);
        $this->form_validation->set_rules('userpassword2', 'Password', 'required|trim|matches[userpassword1]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Registration';
            $this->load->view('../auth/header', $data);
            $this->load->view('v_registration');
            $this->load->view('../auth/footer');
        } else {
            $this->M_auth->addNewUser();
            $this->session->set_flashdata('message', 'Berhasil!');
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button> 
                                You have been <strong>Logged out</strong> !
                            </div>');
        redirect('auth');
    }
}
